#include "stm32g474xx.h"

/**
	* @brief  Инициализация USART1
  * @param  None
  * @retval None
  */
void usart1_init(void) {
	RCC -> APB2ENR |= RCC_APB2ENR_USART1EN; //Включаем тактирование USART1
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOCEN; //Включаем тактирование порта B
	//RCC -> CCIPR |= (1 << 1); //HSI16 тактирование на USART1
	
	//PC5 - RX
	//PC4 - TX
	//Настроим ножки PC5 PC4 для работы USART1
	GPIOC -> MODER &= ~(1 << 8 | 1 << 10); //Альтернативный режим
	//Режим AF7
	GPIOC -> AFR[0] |= (1 << 16 | 1 << 17 | 1 << 18 | 1 << 20 | 1 << 21 | 1 << 22);
	//Высокая скорость для ножек PC5 PC4
	GPIOC -> OSPEEDR |= (1 << 8 | 1 << 9 | 1 << 10 | 1 << 11);
	
	//Настройка UASRT1
	USART1 -> PRESC |= (1 << 0); //input clock /2
	USART1 -> CR1 |= (1 << 0); //uasrt enable
	USART1 -> CR1 |= (1 << 2); //Receiver enable
	USART1 -> CR1 |= (1 << 3); //Transmitter enable
	USART1 -> CR1 |= (1 << 5); //RXNE interrupt enable
	USART1 -> BRR = 0x0D0; // Baudrate 38400
	
	USART1 -> RQR |= (1 << 3); //сбросим флаги USART
	
	//NVIC_EnableIRQ(USART1_IRQn);
	
}
