#include "stm32g474xx.h"

void gpio_init(void) {
	/*___ПОРТ D___*/
	//Включение тактирования порта D
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIODEN;
	
	//PD2 PP mode (тестовый светодиод)
	GPIOD -> MODER &= ~(1 << 5);
	
	/*___ПОРТ B___*/
	//Настроим выходы для управления платой защиты
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN; //Тактирование порта B
	
	//PB4 - Lamp on
	//PB6 - Shield on
	//PB4 PB6 general purpose output
	GPIOB -> MODER |= (1 << 8 | 1 << 12);
	GPIOB -> MODER &= ~(1 << 9 | 1 << 13);
	//PB4 PB6 open drain
	GPIOB -> OTYPER |= (1 << 4 | 1 << 6);
	//PB4 PB6 no pull
	GPIOB -> PUPDR &= ~(1 << 8 | 1 << 9 | 1 << 12 | 1 << 13);
	//PB4 PB6 выключим на всякий случай
	GPIOB -> ODR &= ~(GPIO_ODR_OD4 | GPIO_ODR_OD6);
	
	//PB5 - Сброс фазной защиты (сбрасывается нулём)
	GPIOB -> MODER &= ~(1 << 10 | 1 << 11);
	GPIOB -> MODER |= (1 << 10); //output mode
	GPIOB -> PUPDR |= (1 << 10); //pull up
	GPIOB -> ODR |= GPIO_ODR_OD5; //номинальное состояние еденичка
	
	/*___ПОРТ C___*/
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOCEN; //Тактирование порта C
	
	//PC7 - Shield line IRQ (состояние защиты)
	GPIOC -> MODER &= ~(1 << 14 | 1 << 15); //PC7 input mode
	GPIOC -> OSPEEDR |= (1 << 14 | 1 << 15); //PC7 very high speed
	
	//PC8 - Фазные датчики тока
	GPIOC -> MODER &= ~(1 << 16 | 1 << 17); //PC8 input mode
	GPIOC -> OSPEEDR |= (1 << 16 | 1 << 17); //PC8 very high speed
	
	//PC6 PC9 PC10 - датчики Холла
	//PC10 	- HS1
	//PC9 	- HS2
	//PC6 	- HS3
	
	//PC6 PC9 PC10 input mode
	GPIOC -> MODER &= ~(1 << 12 | 1 << 13 | 1 << 18 | 1 << 19 | 1 << 20 | 1 << 21);
	//PC6 PC9 PC10 very high speed
	GPIOC -> OSPEEDR |= (1 << 12 | 1 << 13 | 1 << 18 | 1 << 19 | 1 << 20 | 1 << 21);
	
}
