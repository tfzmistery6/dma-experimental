#include "stm32g474xx.h"

// Мёртвое время считается по следующей формуле:
// (10^9/16'000'000) * DEADTIME = время в наносекундах (устаревшая инфа)
#define DEADTIME 15

void tim1_init(void) {
	//Инициализация выводов GPIO
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOAEN | RCC_AHB2ENR_GPIOBEN | \
		RCC_AHB2ENR_GPIOCEN; //Включаем тактирование портов А B C
	
	//PC0		-		PP output Hi1
	//PC1		-		PP outout Hi2
	//PA10	-		TIM1_CH3
	//PB13	-		TIM1_CH1N
	//PB0		-		TIM1_CH2N
	//PB9		-		TIM1_CH3N

	//PC0 PC1 режим output mode
	GPIOC -> MODER &= ~(1 << 1 | 1 << 3);
	GPIOC -> MODER |= (1 << 0 | 1 << 2);
	//PC0 PC1 Very high speed
	GPIOC -> OSPEEDR |= (1 << 0 | 1 << 1 | 1 << 2 | 1 << 3);
	GPIOC -> PUPDR |= (1 << 1 | 1 << 3); // PC0 PC1 pull down
	
	//Выключим по инициализации
	GPIOC -> ODR &= ~(GPIO_ODR_OD0);
	GPIOC -> ODR &= ~(GPIO_ODR_OD1);
	
	//PA10 режим AF
	GPIOA -> MODER |= (1 << 21);
	GPIOA -> MODER &= ~(1 << 20);
	//PA10 Very high speed
	GPIOA -> OSPEEDR |= (1 << 20 | 1 << 21);
	//PA10 AF6
	GPIOA -> AFR[1] |= (1 << 9 | 1 << 10);
	
	//PB0 PB9 PB13 режим AF
	GPIOB -> MODER |= (1 << 1 | 1 << 19 | 1 << 27);
	GPIOB -> MODER &= ~(1 << 0 | 1 << 18 | 1 << 26);
	//PB0 PB9 PB13 very high speed
	GPIOB -> OSPEEDR |= (1 << 0 | 1 << 1 | 1 << 18 | 1 << 19 | 1 << 26 | 1 << 27);
	//PB0 AF6
	GPIOB -> AFR[0] |= (1 << 1 | 1 << 2);
	//PB13 AF6
	GPIOB -> AFR[1] |= (1 << 21 | 1 << 22);
	//PB9 AF12
	GPIOB -> AFR[1] |= (1 << 6 | 1 << 7);
	
	//Произведём настройку таймера
	RCC -> APB2ENR |= RCC_APB2ENR_TIM1EN; //Включаем тактирование таймера
	
	TIM1 -> CR1 |= TIM_CR1_ARPE; //Включаем preload для регистра TIM1_ARR
	TIM1 -> CR1 |= TIM_CR1_CMS; //Режим 3 выравнивания по центру
	//Активируем режим PWM 2 и включаем preload для регистра TIM1_CCR
	TIM1 -> CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_0 | TIM_CCMR1_OC1PE; //канал 1
	TIM1 -> CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_0 | TIM_CCMR1_OC2PE; //канал 2
	//TIM1 -> CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_0 | TIM_CCMR2_OC3PE; //канал 3
	//Включаем комплиментарный выход
	TIM1 -> CCER |= TIM_CCER_CC1NE; //канал 1
	TIM1 -> CCER |= TIM_CCER_CC2NE; //канал 2
	//TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3
	TIM1 -> BDTR |= TIM_BDTR_MOE | DEADTIME;
	TIM1 -> PSC = 0; //Делитель частоты TIM1 1 -> частота 16 МГц
	TIM1 -> ARR = 255; //Autoreload (по факту разрешение скважности) //~31 259 Гц
	TIM1 -> CCR1 = 255; //Начальное значение скважности канал 1
	TIM1 -> CCR2 = 255; //Начальное значение скважности канал 2
	//TIM1 -> CCR3 = 255; //Начальное значение скважности канал 3
	TIM1 -> CR1 |= TIM_CR1_CEN;//Включить TIM1
	TIM1 -> EGR |= TIM_EGR_UG; //Сразу генерируем событие обновления
}

//Инициализация таймера 2
//Данный таймер используется для установки
//Токов защит через PWM
void tim2_init(void) {
	//Инициализация выводов GPIO
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOAEN; //Включаем тактирование порта А
	
	//PA15 - TIM2_Ch1 - установка тока защиты
	//PA1 - TIM2_Ch2 - установка тока защиты фазы
	
	//PA15 на ресете стоит в AF режиме
	//PA15 на ресете в режиме Very high speed
	GPIOA -> OTYPER |= (1 << 15); //PA15 открытый коллектор
	GPIOA ->AFR[1] |= (1 << 28); //PA15 AF1 TIM2_Ch1
	
	//PA1
	GPIOA -> MODER &= ~(1 << 2); //режим AF
	//GPIOA -> OTYPER |= (1 << 1); //открытый коллектор
	GPIOA -> AFR[0] |= (1 << 4); //AF1 TIM2_Ch2
	
	
	//Произведём настройку таймера
	RCC -> APB1ENR1 |= RCC_APB1ENR1_TIM2EN; //Включаем тактирование таймера
	
	TIM2 -> CR1 |= TIM_CR1_ARPE; //Включаем preload для регистра TIM2_ARR
	TIM2 -> CR1 |= TIM_CR1_CMS; //Режим 3 выравнивания по центру
	//Активируем режим PWM 2 и включаем preload для регистра TIM2_CCR
	TIM2 -> CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_0 | TIM_CCMR1_OC1PE; //канал 1
	TIM2 -> CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_0 | TIM_CCMR1_OC2PE; //канал 2
	//Включаем основной
	TIM2 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E; //канал 1
	TIM2 -> PSC = 0; //Делитель частоты TIM2 1 -> частота 16 МГц
	TIM2 -> ARR = 255; //Autoreload (по факту разрешение скважности) //~31 259 Гц
	TIM2 -> CCR1 = 0; //Начальное значение скважности канал 1
	TIM2 -> CCR2 = 0; //Начальное значение скважности канал 1
	TIM2 -> CR1 |= TIM_CR1_CEN;//Включить TIM2
	TIM2 -> EGR |= TIM_EGR_UG; //Сразу генерируем событие обновления
}

/** 
 * @brief  Timer 7 initialization.
 * @param  none.
 * @return none.
 * Данный таймер является главным обработчиком системы управления.
 */
void tim7_init(void) {
	RCC -> APB1ENR1 |= RCC_APB1ENR1_TIM7EN; //Включим тактирование TIM7
	
	TIM7 -> SR = 0; //Сбросим статус регистр
	
	//До таймера доходит частота 16 МГц
	//Делитель на (4 + 1) делает частоту тактов 3.2 МГц
	//Переполнение таймера происходит на (199 + 1) тактах, что = 16 кГц
	TIM7 -> PSC = 4; //Предделитель
	TIM7 -> ARR = 399; //Авто-релоад (8 кГц)
	//TIM7 -> ARR = 199; //Авто-релоад (16 кГц)
	//TIM7 -> ARR = 99; //Авто-релоад (32 кГц)
	//TIM7 -> ARR = 49; //Авто-релоад (64 кГц)
	TIM7 -> DIER |= TIM_DIER_UIE; // Включить прерывание по переполнению
	TIM7 -> CR1 = TIM_CR1_CEN; //Включем таймер
	
	//NVIC_EnableIRQ(TIM7_IRQn); //Включить прерывание NVIC
}

/** 
 * @brief  Timer 6 initialization.
 * @param  none.
 * @return none.
 * Данный таймер является обработчиком, в котором контроллер 
 * пытается восстановить работу защиты
 */
void tim6_init(void) {
	RCC -> APB1ENR1 |= RCC_APB1ENR1_TIM6EN; //Включим тактирование TIM6
	
	TIM6 -> SR = 0; //Сбросим статус регистр
	
	//До таймера доходит частота 16 МГц
	//Делитель на (1599 + 1) делает частоту тактов 10 кГц
	//Переполнение таймера происходит на (49 + 1) тактах, что = 200 Гц
	TIM6 -> PSC = 1599; //Предделитель
	TIM6 -> ARR = 49; //Авто-релоад
	TIM6 -> DIER |= TIM_DIER_UIE; // Включить прерывание по переполнению
	TIM6 -> CR1 = TIM_CR1_CEN; //Включем таймер
}

/** 
 * @brief  Timer 5 initialization.
 * @param  none.
 * @return none.
 * Данный таймер является вспомогательным системным обработчиком
 */
void tim5_init(void) {
	RCC -> APB1ENR1 |= RCC_APB1ENR1_TIM5EN; //Включим тактирование TIM5
	
	TIM5 -> SR = 0; //Сбросим статус регистр
	
	//До таймера доходит частота 16 МГц
	//Делитель на (1599 + 1) делает частоту тактов 10 кГц
	//Переполнение таймера происходит на (199 + 1) тактах, что = 50 Гц
	TIM5 -> PSC = 1599; //Предделитель
	TIM5 -> ARR = 199; //Авто-релоад
	TIM5 -> DIER |= TIM_DIER_UIE; // Включить прерывание по переполнению
	TIM5 -> CR1 = TIM_CR1_CEN; //Включем таймер
}



/** 
 * @brief  Timer 15 initialization.
 * @param  none.
 * @return none.
 * Таймер используется для измерения длительности периода сработки
* датчика Холла (измерения скорости вращения)
 */
void tim15_init(void) {
	RCC -> APB2ENR |= RCC_APB2ENR_TIM15EN; //Включим тактирование TIM15
	
	TIM15 -> SR = 0; //Сбросим статус регистр
	
	//До таймера доходит частота 16 МГц
	//Делитель на (1 + 1) делает частоту тактов 8 МГц
	//Переполнение таймера в идеале не должно быть
	TIM15 -> PSC = 1; //Предделитель
	TIM15 -> ARR = 65000; //Авто-релоад
	TIM15 -> CR1 = TIM_CR1_CEN; //Включем таймер
}

/** 
 * @brief  Timer 16 initialization.
 * @param  none.
 * @return none.
 * Этот таймер используется для фиксации факта вращения мотора
 * Если этот таймер доберётся до своего прерывания, то мотор стоит
* Сброс счётчика таймера происходит в прерывании датчика холла
 */
void tim16_init(void) {
	RCC -> APB2ENR |= RCC_APB2ENR_TIM16EN; //Включим тактирование TIM16
	
	TIM16 -> SR = 0; //Сбросим статус регистр
	
	//До таймера доходит частота 16 МГц
	//Делитель на (499+1) делает частоту тактов 32 кГц
	//Переполнение таймера происходит на (63 999 + 1) тактах, что даёт период 2 сек
	TIM16 -> PSC = 499; //Предделитель
	TIM16 -> ARR = 31999; //Авто-релоад
	TIM16 -> DIER |= TIM_DIER_UIE; // Включить прерывание по переполнению
	TIM16 -> CR1 = TIM_CR1_CEN; //Включем таймер
}


