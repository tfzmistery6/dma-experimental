#include "stm32g474xx.h"

/* PROTOTYPES */
void SysTick_init(void);
void SysTick_wait(uint32_t delay);
