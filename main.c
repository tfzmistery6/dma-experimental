/******************************************************************************
 * @file    main.c
 * @brief   Главный файл тестирования новых модулей MCU в серии STM32G4
 * @version v0.1
 * @date    22.09.2021
 * @author  Власовский Алексей Игоревич
 * @note    АО "ОКБ МЭЛ" г.Калуга
 ******************************************************************************/

#include "stm32g474xx.h"
#include "SysTick_.h"
#include "var_.h"
#include "gpio_.h"
#include "usart_.h"
#include "tim_.h"
#include "adc_.h"
#include "exti_.h"
#include "rcc_.h"
#include "dma_.h"

/*____________________PROTOTYPES____________________*/
void global_var_init(void); //дополнительная инициализация переменных
/*___Коммуникации___*/
void clear_RXBuffer(void); //Очистка буфера принятых символов
static char *utoa_cycle_sub(uint32_t value, char *buffer); //Преобразование uint to ascii
char * utoa_div(uint32_t value, char *buffer); //преобразование uint to ascii через %
int is_equal(char *string1, char *string2, int len); //Функция сравнения двух строк (замена strncmp)
int is_digit(char byte); //функция проверки является ли принятый байт цифрой в ascii кодировке
int get_param(char *buf); //выделение параметров из буфера
void USART1_TXBuf_append (char *buffer); //Отправка данных в USART1
void RXBuffer_Handler (void); //Обработчик буфера принятых символов после приёма CR

int main() {
	//"включение FPU"
	SCB -> CPACR |= (3 << 20 | 3 << 22); //CP10 CP11 Full access
	sysclk_cfgr();
	
	global_var_init();
	gpio_init();
	//usart1_init();
	dma1_init();
	adc1_init();//Измерение тока
	adc2_init();//измерение температуры
	adc3_init();//Измерение напряжения питания
	//tim7_init();
	//tim5_init();
	tim15_init();
	
	//__disable_irq(); //для начала выключим прерывания
	//NVIC_EnableIRQ(TIM7_DAC_IRQn);
	//NVIC_EnableIRQ(TIM5_IRQn);
	//NVIC_EnableIRQ(ADC1_2_IRQn);
	//NVIC_EnableIRQ(ADC3_IRQn);
	//NVIC_EnableIRQ(USART1_IRQn);
	//__enable_irq(); //Включим прерывания
	
	value32 = 9999999ul;

	TIM15 -> CNT = 0;
	utoa_cycle_sub(value32, buffer_);
	busy_time = TIM15 -> CNT;
	
	value32 = 9999999ul;
	
	TIM15 -> CNT = 0;
	TIM15 -> CNT = 0;
	utoa_div(value32, buffer_);
	busy_time = TIM15 -> CNT;
	
	TIM15 -> CNT = 0;
	while(1);
}

/*_______________INTERRUPTS HANDLERS_______________*/

//Прерывание по переполнению таймера TIM7 (32 кГц)
void TIM7_DAC_IRQHandler (void) {
	TIM7 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	tim7_counter++;
	if (tim7_counter == 400)
	{
		tim7_counter = 0;
//		USART1_TXBuf_append(utoa_cycle_sub(ADC1_data[0], buffer_));
//		USART1_TXBuf_append(" ");
//		USART1_TXBuf_append(utoa_cycle_sub(ADC1_data[1], buffer_));
//		USART1_TXBuf_append(" ");
//		USART1_TXBuf_append(utoa_cycle_sub(ADC1_data[2], buffer_));
//		USART1_TXBuf_append("\n");
	}
}

//Вспомогательный таймер (200 Гц)
void TIM5_IRQHandler (void) {
	TIM5 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	RXBuffer_Handler();
}

//Прерывание USART1
void USART1_IRQHandler(void) {
	//data is transferred to the shift register
	if (USART1 -> ISR & USART_ISR_TXE) {
		if (TXi_t != TXi_w) {
			USART1 -> TDR = TX_BUF[TXi_t];
			TXi_t ++;
			TX_BUF_empty_space++;
	
			if (TXi_t >= 256){ TXi_t = 0; }
		} else {
			USART1 -> CR1 &= ~USART_ISR_TXE; //Запретить прерывания на передачу
		}
	}
	
	//проверяем что был принят бит
	if (USART1 -> ISR & USART_ISR_RXNE){
		RXc = USART1 -> RDR;
		RX_BUF[RXi] = RXc;
		RXi++;

		//Если не символ возврата каретки
		if (RXc != 13) {
			if (RXi > RX_BUF_SIZE-1) {
				clear_RXBuffer();
			}
		} else {
			RX_FLAG_END_LINE = 1;
		}
	}
}

////Прерывание ADC1 и ADC2 по флагу EOC
//void ADC1_2_IRQHandler (void) {
//	if (ADC1 -> ISR & ADC_ISR_EOC) {
//		//Читаем очередные данные в последовательности
//		//преобразований, отслеживаемых через ADC1_i
//		ADC1_data[ADC1_i] = ADC1 -> DR;
//		ADC1_i++;
//		if(ADC1_i == 3) {
//			ADC1_i = 0;
//		}
//	}
//}

////Прерывание ADC3 по флагу EOC
//void ADC3_IRQHandler(void) {
//	ADC3_data = ADC3 -> DR;
//}

void Reset_Handler (void) {
	main();
}

/*____________________FUNCTIONS____________________*/

/**
	* @brief  Инициализация переменных
  * @param  None
  * @retval None
  */
void global_var_init(void) {
	clear_RXBuffer(); //очистим буфер перед работой
	
	TXi_t = 0;
	TXi_w = 0;
	TX_BUF_empty_space = 256; //При инициализациии буфер пуст
	
	//Сбросим указатели номера преобразования АЦП
	ADC1_i = 0;
	
	tim7_counter = 0;
}

/*___Функциональные блоки___*/

/*___Коммуникации___*/
//очистка буфера приёмника
void clear_RXBuffer(void) {
	for (RXi=0; RXi<RX_BUF_SIZE; RXi++)
		RX_BUF[RXi] = '\0';
	RXi = 0;
	
	input_param[0] = 0;
	input_param[1] = 0;
}
/**
	* @brief  Запись данных в циклический буфер на отправку через USART
	* @param  Массив данных, предназначенных на отправку
  * @retval None
  */
void USART1_TXBuf_append (char *buffer) {
	//Пока в буфере есть данные
	while (*buffer) {
		TX_BUF[TXi_w] = *buffer++;
		TXi_w++;
		TX_BUF_empty_space--;
		
		if (TXi_w >= 256){ TXi_w = 0; }
	}
	
	USART1 -> CR1 |= (1 << 7); //разрешить прерывание на передачу
	
	//блок ниже вроде как не нужен, потому что флаг TXE не может быть сброшен никогда
	/*
	//Если USART ничего не отправляет, то необходимо отправить один бит
	if (USART1 -> ISR & (1 << 7)) //data is transferred to the shift register
	{
		USART1 -> TDR = TX_BUF[TXi_t];
		TXi_t ++;
		TX_BUF_empty_space++;
	}
	*/
	
	if (TXi_t >= 256){ TXi_t = 0; }
}

/**
	* @brief  Перевод числа в аски символы методом циклического вычитания
	* @param  value - число, которое необходимо преобразовать
	*					*buffer - адрес массив, куда писать результат
  * @retval Возвращает буфер с записанным результататом преобразования
  */
static char * utoa_cycle_sub(uint32_t value, char *buffer) {
   if(value == 0) {
      buffer[0] = '0';
      buffer[1] = 0;
      return buffer;
   }
   char *ptr = buffer;
   uint8_t i = 0;
   do {
      uint32_t pow10 = pow10Table32[i++];
      uint8_t count = 0;
      while(value >= pow10) {
         count ++;
         value -= pow10;
      }
      *ptr++ = count + '0';
   } while(i < 10);
   *ptr = 0;
	// удаляем ведущие нули
		while(buffer[0] == '0') {
		++buffer;
		}
   return buffer;
}

/**
	* @brief  Перевод числа в аски символы методом циклического вычитания
	* @param  value - число, которое необходимо преобразовать
	*					*buffer - адрес массив, куда писать результат
  * @retval Возвращает буфер с записанным результататом преобразования
  */
char * utoa_div(uint32_t value, char *buffer){
   buffer += 11; 
		// 11 байт достаточно для десятичного представления 32-х байтного числа
		// и  завершающего нуля
   *--buffer = 0;
   do
   {
      *--buffer = value % 10 + '0';
      value /= 10;
   }
   while (value != 0);
   return buffer;
}

/**
	* @brief  Функция проверяет по таблице ascii является ли байт цифрой
	* @param  byte - первая строка
	* @retval Возвращает 1, если байт является цифрой
  */
int is_digit(char byte) {
	if ((byte == 0x30) | (byte == 0x31) | (byte == 0x32) | (byte == 0x33) |\
		(byte == 0x34) | (byte == 0x35) | (byte == 0x36) | (byte == 0x37) |\
	(byte == 0x38) | (byte == 0x39)) {
		return 1;
	} else {
		return 0;
	}
};

/**
	* @brief  Функция сравнения двух строк (замена strncmp)
	* @param  string1 - первая строка
	*					string2 - вторая строка
	*					len - длинна сравнения (количество байт)
	* @retval Возвращает 0, если строки равны
  */
int is_equal(char *string1, char *string2, int len) {
	while(len --> 0) {
		if(*string1++ != *string2++) {
			return 1;
		}
	}
	return 0;
};

/**
	* @brief  Поиск в буфере чисел как параметров команды
	* @param  *buf - адрес массива на приём
	* @retval Возвращает 0 при возникновении ошибки
  */
int get_param(char *buf) {
	uint8_t num_of_params = 2;
	uint8_t i = 0;
	uint8_t pos = 0;
	int value = 0;
	
	//Дойдём до первого пробела
	while(buf[pos] != ' ') {
		pos++;
		//Если прошли весь буфер и не нашли пробела
		if (pos >= RX_BUF_SIZE) {
			//Выходим из функции
			return 0;
		}
	}
	
	//Начиная со следующего символа начнём вычислять параметры
	while(buf[++pos] != '\r') {
		if (!(is_digit(buf[pos]) | (buf[pos] == ' '))) {
			USART1_TXBuf_append("Read error!\r");
			clear_RXBuffer();
			return 0;
		}
		
		if (buf[pos] != ' ') {
			value = value*10 + (buf[pos] - 48);
		} else {
			input_param[i] = value;
			i++;
			value = 0;
			if (i > num_of_params -1) {
				USART1_TXBuf_append("Read error!\r");
				clear_RXBuffer();
				return 0;
			}
		}
	}
	
	//Запишем параметр, который будет перед концом строки
	input_param[i] = value;
	
	return 1;
};

//Обработка буфера приёма
void RXBuffer_Handler (void) {
	//Если что-то было считано в буфер команды
	if (RX_FLAG_END_LINE == 1) {
		
		//Вычислим параметры у команды
		get_param(RX_BUF);
		
		// Reset END_LINE Flag
		RX_FLAG_END_LINE = 0;
		
		//Полный останов
		if (is_equal(RX_BUF, "test\r", 6) == 0) {
			USART1_TXBuf_append("jopa.\n");
		}
				
		clear_RXBuffer();
	}
}
