#include "stm32g474xx.h"

//буфер считываемой команды
#define RX_BUF_SIZE 80
char RX_FLAG_END_LINE = 0; //флаг принятия конца строки
char RXi; //указатель на элеммент массива на чтение
char RXc; //буфер для читаемого символа
char RX_BUF[RX_BUF_SIZE] = {'\0'}; //массив на чтение

uint16_t input_param[2]; //Буфер параметров команды принятой в USART1

char buffer_[12]; //Буфер для чисел, которые необходимо вывести в USART1
uint32_t busy_time;
uint32_t value32;

//Циклический буфер на отправку
#define TX_BUF_SIZE 256
int TX_BUF_empty_space; //Колчиство свободных байт в TX_BUF
int TXi_w; //Указатель куда писать байт в массив
int TXi_t; //Указатель откуда отправлять байт в массив
char TX_BUF[TX_BUF_SIZE] = {'\0'}; //Циклический массив для отправки данных по USART

//Измерения по АЦП
uint16_t phase_current[3]; //Фазные токи (АЦП1)
uint16_t temperature[4]; //Темпертуры мотора и радиаторов (АЦП2)
uint16_t sup_voltage; //Напряжение питания в единицах (АЦП3)

uint16_t ADC1_i; //Счётчик флагов EOC для правильного распределения данных из АЦП1

//Таблица степеней десятки
const uint32_t  pow10Table32[]=
{
    1000000000ul,
    100000000ul,
    10000000ul,
    1000000ul,
    100000ul,
    10000ul,
    1000ul,
    100ul,
    10ul,
    1ul
};

uint16_t tim7_counter;