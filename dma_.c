#include "stm32g474xx.h"

void dma1_init(void) {
	extern uint16_t phase_current[3]; //������ ���� (���1)
	extern uint16_t temperature[4]; //���������� ������ � ���������� (���2)
	extern uint16_t sup_voltage; //���������� ������� � �������� (���3)
	
	//������� ������������ DMA1 � DMAMUX1
	RCC -> AHB1ENR |= RCC_AHB1ENR_DMA1EN;
	RCC -> AHB1ENR |= RCC_AHB1ENR_DMAMUX1EN;
	
	//����� 1 - ������ � ADC1 (��������� ������ �����)
	DMA1_Channel1 -> CPAR = (uint32_t)&(ADC1 -> DR); //����� ���������
	DMA1_Channel1 -> CMAR = (uint32_t)phase_current; //����� ������
	DMA1_Channel1 -> CNDTR = 3; //����� ������ ��� ��������
	
	DMA1_Channel1 -> CCR |= DMA_CCR_CIRC; //������� �������� �����
	DMA1_Channel1 -> CCR |= DMA_CCR_MINC; //����� ���������� ��� ������
	DMA1_Channel1 -> CCR |= (1 << 8); //������ ������� � ��������� 16 ���
	DMA1_Channel1 -> CCR |= (1 << 10); //������ ������� � ������ 16 ���
	
	DMAMUX1_Channel0 -> CCR |= 5; //ADC1 ������������� 5-�� �������	
	DMA1_Channel1 -> CCR |= DMA_CCR_EN; //�������� DMA
	
	//����� 2 - ������ � ADC2 (��������� ����������)
	DMA1_Channel2 -> CPAR = (uint32_t)&(ADC2 -> DR); //����� ���������
	DMA1_Channel2 -> CMAR = (uint32_t)temperature; //����� ������
	DMA1_Channel2 -> CNDTR = 4; //����� ������ ��� ��������
	
	DMA1_Channel2 -> CCR |= DMA_CCR_CIRC; //������� �������� �����
	DMA1_Channel2 -> CCR |= DMA_CCR_MINC; //����� ���������� ��� ������
	DMA1_Channel2 -> CCR |= (1 << 8); //������ ������� � ��������� 16 ���
	DMA1_Channel2 -> CCR |= (1 << 10); //������ ������� � ������ 16 ���
	
	DMAMUX1_Channel1 -> CCR |= 36; //ADC2 ������������� 36-�� �������	
	DMA1_Channel2 -> CCR |= DMA_CCR_EN; //�������� DMA
	
	//����� 3 - ������ � ADC3 (��������� ���������� �������)
	DMA1_Channel3 -> CPAR = (uint32_t)&(ADC3 -> DR); //����� ���������
	DMA1_Channel3 -> CMAR = (uint32_t)&sup_voltage; //����� ������
	DMA1_Channel3 -> CNDTR = 1; //����� ������ ��� ��������
	
	DMA1_Channel3 -> CCR |= DMA_CCR_CIRC; //������� �������� �����
	DMA1_Channel3 -> CCR |= (1 << 8); //������ ������� � ��������� 16 ���
	DMA1_Channel3 -> CCR |= (1 << 10); //������ ������� � ������ 16 ���
	
	DMAMUX1_Channel2 -> CCR |= 37; //ADC3 ������������� 37-�� �������	
	DMA1_Channel3 -> CCR |= DMA_CCR_EN; //�������� DMA
}