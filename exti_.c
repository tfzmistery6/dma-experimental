#include "stm32g474xx.h"

//Настройка контроллера внешних прерываний
void exti_init(void) {
	
	RCC -> APB2ENR |= RCC_APB2ENR_SYSCFGEN;//Включаем SYSCFG
	SYSCFG -> EXTICR[1] |= (1 << 13); //EXTI7 - PC7 Shield
	SYSCFG -> EXTICR[2] |= (1 << 9); //EXTI10 - PC10 HS1
	SYSCFG -> EXTICR[2] |= (1 << 1); //EXTI8 - PC8 Датчики тока фазы
	
	//Настройка контроллера внешних прерываний
	//EXTI7
	EXTI -> FTSR1 |= EXTI_FTSR1_FT7; //falling trigger event
	EXTI -> PR1 |= EXTI_PR1_PIF7; //сбрасываем прерывание перед самим прерыванием
	EXTI -> IMR1 |= EXTI_IMR1_IM7; //включаем прерывание
	//EXTI8
	EXTI -> RTSR1 |= EXTI_RTSR1_RT8; //rising trigger event
	EXTI -> PR1 |= EXTI_PR1_PIF8; //сбрасываем прерывание перед самим прерыванием
	EXTI -> IMR1 |= EXTI_IMR1_IM8; //включаем прерывание
	//EXTI10
	EXTI -> RTSR1 |= EXTI_RTSR1_RT10; //rising trigger event
	//EXTI -> FTSR1 |= EXTI_FTSR1_FT10; //falling trigger event
	EXTI -> PR1 |= EXTI_PR1_PIF10; //сбрасываем прерывание перед самим прерыванием
	EXTI -> IMR1 |= EXTI_IMR1_IM10; //включаем прерывание
};
