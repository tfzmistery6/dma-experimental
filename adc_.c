#include "stm32g474xx.h"

void adc1_init(void) {
	/*___________ADC1___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PA0 - ADC1_In1
	* PB14 - ADC1_In5
	* PC2 - ADC12_In8
	*/
	//Включим тактирование GPIO
	RCC -> AHB2ENR |= (RCC_AHB2ENR_GPIOAEN | RCC_AHB2ENR_GPIOCEN | RCC_AHB2ENR_GPIOBEN);
	//Выводы после ресета настроены на аналоговый режим
	
	//Включим тактирование ADC1 и ADC2
	RCC -> AHB2ENR |= RCC_AHB2ENR_ADC12EN;
	ADC12_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC1 -> CR = 0;//Сбросим регистр CR
	ADC1 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	for (int i = 0; i <= 120; i++){GPIOB -> ODR ^= GPIO_ODR_OD11;}
	GPIOB -> ODR &= ~(1 << 11);
	
	ADC1 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC1 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC1 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC1 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC1
	ADC1 -> CFGR |= (1 << 13); //continuous mode
	ADC1 -> SQR1 |= (0x2 << 0); // L = 3 преобразований (прим. 0000 == 1 пробразование)
	ADC1 -> SQR1 |= (0x1 << 6); //SQ1 = In1
	ADC1 -> SQR1 |= (0x5 << 12); //SQ2 = In5
	ADC1 -> SQR1 |= (0x8 << 18); //SQ3 = In8
	
	ADC1 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 8) | (1 << 7); //SMP2 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 11) | (1 << 10); //SMP3 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 14) | (1 << 13); //SMP4 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 17) | (1 << 16); //SMP5 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 20) | (1 << 19); //SMP6 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 23) | (1 << 22); //SMP7 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 26) | (1 << 25); //SMP8 == 181.5 ticks
	ADC1 -> SMPR1 |= (1 << 29) | (1 << 28); //SMP9 == 181.5 ticks
	
	ADC1 -> SMPR2 |= (1 << 2) | (1 << 1); //SMP10 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 5) | (1 << 4); //SMP11 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 8) | (1 << 7); //SMP12 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 11) | (1 << 10); //SMP13 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 14) | (1 << 13); //SMP14 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 17) | (1 << 16); //SMP15 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 20) | (1 << 19); //SMP16 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 23) | (1 << 22); //SMP17 == 181.5 ticks
	ADC1 -> SMPR2 |= (1 << 26) | (1 << 25); //SMP18 == 181.5 ticks
	
	ADC1 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	// Разрешим работу с DMA в кольцевом режиме
	ADC1 -> CFGR |= (ADC_CFGR_DMAEN | ADC_CFGR_DMACFG);
	
	//ADC1 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC1_2_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC1_2_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC1 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
}

void adc2_init(void) {
	/*___________ADC2___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PA6 - ADC2_In3
	* PA7 - ADC2_In4
	* PB2 - ADC2_In12
	* PA5 - ADC2_In13
	*/
	//Включим тактирование GPIO
	RCC -> AHB2ENR |= (RCC_AHB2ENR_GPIOAEN | RCC_AHB2ENR_GPIOBEN);
	//Выводы после ресета настроены на аналоговый режим
	
	//Включим тактирование ADC1 и ADC2
	RCC -> AHB2ENR |= RCC_AHB2ENR_ADC12EN;
	ADC12_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC2 -> CR = 0;//Сбросим регистр CR
	ADC2 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	for (int i = 0; i <= 120; i++){GPIOB -> ODR ^= GPIO_ODR_OD11;}
	GPIOB -> ODR &= ~(1 << 11);
	
	ADC2 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC2 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC2 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC2 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC1
	ADC2 -> CFGR |= (1 << 13); //continuous mode
	ADC2 -> SQR1 |= (0x3 << 0); // L = 4 преобразований (прим. 0000 == 1 пробразование)
	ADC2 -> SQR1 |= (3 << 6); //SQ1 = In3
	ADC2 -> SQR1 |= (4 << 12); //SQ2 = In4
	ADC2 -> SQR1 |= (12 << 18); //SQ3 = In12
	ADC2 -> SQR1 |= (13 << 24); //SQ4 = In13
	
	ADC2 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 8) | (1 << 7); //SMP2 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 11) | (1 << 10); //SMP3 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 14) | (1 << 13); //SMP4 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 17) | (1 << 16); //SMP5 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 20) | (1 << 19); //SMP6 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 23) | (1 << 22); //SMP7 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 26) | (1 << 25); //SMP8 == 181.5 ticks
	ADC2 -> SMPR1 |= (1 << 29) | (1 << 28); //SMP9 == 181.5 ticks
	
	ADC2 -> SMPR2 |= (1 << 2) | (1 << 1); //SMP10 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 5) | (1 << 4); //SMP11 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 8) | (1 << 7); //SMP12 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 11) | (1 << 10); //SMP13 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 14) | (1 << 13); //SMP14 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 17) | (1 << 16); //SMP15 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 20) | (1 << 19); //SMP16 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 23) | (1 << 22); //SMP17 == 181.5 ticks
	ADC2 -> SMPR2 |= (1 << 26) | (1 << 25); //SMP18 == 181.5 ticks
	
	ADC2 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	// Разрешим работу с DMA в кольцевом режиме
	ADC2 -> CFGR |= (ADC_CFGR_DMAEN | ADC_CFGR_DMACFG);
	//ADC2 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC1_2_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC1_2_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC2 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
}

void adc3_init(void) {
/*___________ADC3___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PB1 - ADC3_In1 - Входное напряжение
	*/
	//Включим тактирование GPIO
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
	
	//Включим тактирование ADC3 и ADC4
	RCC -> AHB2ENR |= RCC_AHB2ENR_ADC345EN;
	ADC345_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC3 -> CR = 0;//Сбросим регистр CR
	ADC3 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	//Просто подёргаем битом внутри цикла
	for (int i = 0; i <= 120; i++){GPIOB -> ODR ^= GPIO_ODR_OD11;}
	GPIOB -> ODR &= ~(1 << 11);
	
	ADC3 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC3 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC3 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC3 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC3
	ADC3 -> CFGR |= (1 << 13); //continuous mode
	ADC3 -> SQR1 |= (0x0 << 0); // L = 1 преобразований (прим. 0000 == 1 пробразование)
	ADC3 -> SQR1 |= (0x1 << 6); //SQ1 = In1

	ADC3 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks

	ADC3 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	// Разрешим работу с DMA в кольцевом режиме
	ADC3 -> CFGR |= (ADC_CFGR_DMAEN | ADC_CFGR_DMACFG);
	
	//ADC3 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC3_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC3_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC3 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
};

void adc4_init(void) {
/*___________ADC4___________*/
	
	//Перед инициализацией АЦП настроим входы в аналоговый режим
	/*Выдержка из таблицы:
	* PB15 - ADC4_In5 - Ручка потенциометра
	*/
	//Включим тактирование GPIO
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
	
	//Включим тактирование ADC3 и ADC4
	RCC -> AHB2ENR |= RCC_AHB2ENR_ADC345EN;
	ADC345_COMMON -> CCR |= (1 << 16); //Источник тактирования  HCLK/1 (Synchronous clock mode)
	
	//Проведём калибровку модуля АЦП
	ADC4 -> CR = 0;//Сбросим регистр CR
	ADC4 -> CR |= (1 << 28); //ADC Voltage regulator enabled
	
	//Вставим задержку для запуска ADVREGEN
	//Просто подёргаем битом внутри цикла
	for (int i = 0; i <= 120; i++){GPIOB -> ODR ^= GPIO_ODR_OD11;}
	GPIOB -> ODR &= ~(1 << 11);
	
	ADC4 -> CR |= ADC_CR_ADCAL; //Запускаем калибровку
	while((ADC4 -> CR & ADC_CR_ADCAL) != 0); //Дожиддаемся окончания калибровки (ADCAL == 0)
	
	ADC4 -> CR |= ADC_CR_ADEN; //Включим АЦП
	while (!(ADC4 -> ISR & (1 << 0))); //Дожидаемся флага ADRDY
		
	//Произведём настройку ADC3
	ADC4 -> CFGR |= (1 << 13); //continuous mode
	ADC4 -> SQR1 |= (0x0 << 0); // L = 1 преобразований (прим. 0000 == 1 пробразование)
	ADC4 -> SQR1 |= (0x5 << 6); //SQ1 = In5

	ADC4 -> SMPR1 |= (1 << 5) | (1 << 4); //SMP1 == 181.5 ticks

	ADC4 -> CFGR |= ADC_CFGR_AUTDLY; //ADUTODELAY mode enable
	
	// Разрешим работу с DMA в кольцевом режиме
	ADC4 -> CFGR |= (ADC_CFGR_DMAEN | ADC_CFGR_DMACFG);
	
	//ADC4 -> IER |= ADC_IER_EOCIE; //End of regular conversation interrupt enable
	//NVIC_EnableIRQ(ADC3_IRQn); //Включить вектор прерываний
	//NVIC_SetPriority(ADC3_IRQn, 2); //Понижаем приоритет прерывания
	
	ADC4 -> CR |= ADC_CR_ADSTART; //Старт регулярных преобразований
};